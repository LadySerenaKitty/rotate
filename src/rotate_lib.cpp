#include "rotate.h"

#ifdef __cplusplus
extern "C" {
#endif

void rotate(unsigned char key, std::istream &is, std::ostream &os) {
	unsigned char chr;
	while(is.good()) {
		chr = is.get();
		if (is.fail()) { return; }
		os << (unsigned char)((chr + key) % 0xff);
	}
}

void unrotate(unsigned char key, std::istream &is, std::ostream &os) {
	unsigned char xkey = 0xff - key;
	rotate(xkey, is, os);
}

#ifdef __cplusplus
}
#endif

