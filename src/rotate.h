#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

void rotate(unsigned char key, std::istream &is, std::ostream &os);
void unrotate(unsigned char key, std::istream &is, std::ostream &os);

#ifdef __cplusplus
}
#endif

