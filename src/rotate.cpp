#include "rotate.h"
#include <fstream>
#include <regex>
#include <string>

int main(int argc, char** argv) {

	unsigned char key = 13;
	bool isfile = false;

	std::istream *is = &std::cin;
	std::string filename;

	std::regex re("([a-z]+)=(.*)");
	for(int a = 1; a < argc; a++) {
		std::string pt(argv[a]);
		std::smatch m;
		if (std::regex_match(pt, m, re)) {
			std::string pt = m[1];
			std::string val = m[2];
			if (!pt.empty() && !val.empty()) {
				if (pt == std::string("key")) {
					int radix = 10;
					switch (argv[1][0]) {
						case 'x': radix = 16; break;
						case 'o': case '0': radix = 8; break;
						case 'b': key = radix = 2; break;
					}
					key = (unsigned char)std::stoi(m[2], nullptr, radix);
				}
				if (pt == std::string("file")) {
					isfile = true;
					filename = m[2];
				}
			}
		}
	}

	if (isfile) {
		std::ifstream is(filename, std::ios::binary | std::ios::in);
		rotate(key, is, std::cout);
		is.close();
	}
	else { rotate(key, std::cin, std::cout); }

	return 0;
}

