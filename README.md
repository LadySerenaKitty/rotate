# Rotate

The rot13 cipher, expanded and digitized.  Works on all bytes, not just the subset constituting the latin alphabet.
+ **rotate** - Rotates input
+ **etator** - Unrotates input (decodes)

## Installation
[![Packaging status](https://repology.org/badge/vertical-allrepos/rotate.svg)](https://repology.org/project/rotate/versions)

### FreeBSD
Ports:
```
cd /usr/local/security/rotate
make install clean
```
Packages:
```
pkg install rotate
```

## Usage
Usage is simple and straightforward.  Without any arguments, it will read from `stdin`.  All output goes to `stdout`,
so if you want the output to go to a file, use shell redirection.

### Arguments
+ `file=filename`: If present, reads from file.  Omit to use `stdin`
+ `key=64`: If present, sets the key size.  Valid values are 0-255, defaults to 13 if omitted.

